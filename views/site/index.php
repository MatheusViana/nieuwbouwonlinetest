<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to NieuwbouwOnline Test!</h1>

        <p class="lead">Click to verify the price tag of your house.</p>

        <p><a class="btn btn-lg btn-success" href="?r=userhouse/index">Try it out</a></p>
    </div>
</div>
