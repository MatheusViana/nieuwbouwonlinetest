<div class="container">

    <?php if (Yii::$app->session->hasFlash('noHousesFound')): ?>

            <div class="alert alert-danger">
                Sorry we couldn`t find any similar house to compare
            </div>

    <?php else: ?>

        <?php if (Yii::$app->session->hasFlash('emailSubmitted')): ?>
            <div class="alert alert-success">
                This report was also submitted to your email.
            </div>
        <?php endif; ?>

        <h3>Hello <?= $searchInfo->firstname.' '.$searchInfo->lastname ?>, your house price is around $<?= $houseprice ?>
                with an avarage of $<?= $pricePerFloor ?> per m<sup>2</sup></h3>
        <br>
        <div class="row">
                <div class="col-md-6">
                    <div class="media">
                        <div class="media-body">
                            <h4 class="media-heading">House Info</h4>

                            <span><b>Postal Code:</b> <?= $searchInfo->postalcode ?></span><br>
                            <span><b>House Number:</b> <?= $searchInfo->housenumber ?></span><br>
                            <span><b>Addition:</b> <?= $searchInfo->addition ?></span><br>          
                            <span><b>Square Meters:</b> <?= $searchInfo->squaremeter ?></span><br>
                        </div>
                    </div>
                </div>       
        </div>

        <h3>Similar Houses</h3>
        <div class="row">
            <?php foreach($houses as $key => $house) : ?>    
                <div class="col-md-6">
                    <div class="media">
                    <div class="media-left">
                        <a href="#">
                        <img class="media-object" src="<?= $house->hose_img ?>" alt="..." style="width:200px;height:130px">
                        </a>
                    </div>
                    <div class="media-body">
                        <span class="pull-right">$<?= $house->price ?></span>
                        <h4 class="media-heading"><?= $house->city ?></h4>    

                        <span><b>Street:</b> <?= $house->streetname ?></span><br>
                        <span><b>House Number:</b> <?= $house->housenumber ?></span><br>
                        <span><b>Addition:</b> <?= $house->addition ?></span><br>               
                        <span class="pull-right"><?= $house->status ?></span>
                        <span><b>Floor m2:</b> <?= $house->floor ?></span><br>                    
                    </div>
                    </div>
                </div>
            <?php endforeach ?>        
        </div>

    <?php endif; ?>
</div>