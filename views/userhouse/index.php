<?php 
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Html;
?>

<h2>Calculate the value of your house</h2>

<hr>

<?php $form = ActiveForm::begin() ?>

    <h3>House Info</h3>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'postalcode') ?>
        </div>    
        <div class="col-md-4">
            <?= $form->field($model, 'housenumber') ?>
        </div>    
        <div class="col-md-4">
            <?= $form->field($model, 'housenumberaddition') ?>
        </div>        
        <div class="col-md-4">
            <?= $form->field($model, 'squaremeter') ?>
        </div> 
        <div class="col-md-4">
            <?= $form->field($model, 'typeofhouse')->dropDownList([
                '0' => 'Appartment',
                '1' => 'Detached​ ​House',
                '2' => 'Semi​ ​Detached​ ​House',
                '3' => 'Terraced​ ​House',
                '4' => 'Corner​ ​House',
            ],
            ['prompt'=>'Select Option']); ?>
        </div> 
    </div>    

    <h3>Your motivation</h3>   
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'reason')->dropDownList([
                    '0' => 'I ​want​ ​to​ ​sell​ ​my​ ​house', 
                    '1' => 'I ​want​ ​to​ ​buy​ ​this​ ​house',
                    '2' => 'I’m​ ​just​ ​curious​ ​about​ ​the​ ​price', 
                ],
                ['prompt'=>'Select Option']); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'timetomove')->dropDownList([
                    '0' => 'As​ ​fast​ ​as​ ​possible', 
                    '1' => 'Within​ ​3​ ​months',
                    '2' => 'Within​ ​6​ ​months', 
                    '3' => 'Within​ ​1​ ​year', 
                ],
                ['prompt'=>'Select Option']); ?>
        </div>
    </div>

    <h3>Personal Information</h3>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'firstname') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'lastname') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'addition') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'phonenumber') ?>
        </div>
    </div>    
    <hr>    

    <h3>Presentation</h3>
    <hr>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'presentedoption')->dropDownList([
                    '0' => 'Send me an e-mail',
                    '1' => 'Just show me an PDF',                     
                ],
                ['prompt'=>'Select Option']); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary pull-right']) ?>
    </div>

<?php ActiveForm::end() ?>