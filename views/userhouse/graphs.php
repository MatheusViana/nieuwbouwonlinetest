<?php

use yii\helpers\Json;

?>

<h1>Houses X Floor m<sup>2</sup></h1>
<canvas id="houses-floor"></canvas>

<h1>Houses X Price</h1>
<canvas id="houseprice-floor"></canvas>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<?php
$script = <<< JS

    function numberConvert(item,index) {
        return Number(item);
    }

    $.ajax({ 
        type: "POST", 
        url: "?r=userhouse/housesfloor",
        data: {'data' : 4234},
        success: function(msg){

            let responseJson = JSON.parse(msg);            

            var ctx = document.getElementById('houses-floor').getContext('2d');
            // responseJson.data = responseJson.data.map(numberConvert);

            // console.log(responseJson);

            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',      
                          

                // The data for our dataset
                data: {
                    labels: responseJson.labels,
                    datasets: [{
                        label: "Number of houses",
                        // backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(92, 184, 92)',
                        data: responseJson.data,
                    }]
                },

                // Configuration options go here
                options: {}
            });

        },
        error: function(err){   
            console.log(err); 
        } 
    });

    $.ajax({ 
        type: "POST", 
        url: "?r=userhouse/houseprice",
        data: {'data' : 4234},
        success: function(msg){

            let responseJson = JSON.parse(msg);            

            var ctx = document.getElementById('houseprice-floor').getContext('2d');
            // responseJson.data = responseJson.data.map(numberConvert);

            // console.log(responseJson);

            var chart = new Chart(ctx, {
                // The type of chart we want to create
                type: 'line',      
                          

                // The data for our dataset
                data: {
                    labels: responseJson.labels,
                    datasets: [{
                        label: "Number of houses",
                        // backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(92, 184, 92)',
                        data: responseJson.data,
                    }]
                },

                // Configuration options go here
                options: {}
            });

        },
        error: function(err){   
            console.log(err); 
        } 
    });

JS;
$this->registerJs($script);