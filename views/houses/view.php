<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Houses */

$this->title = $model->house_id;
$this->params['breadcrumbs'][] = ['label' => 'Houses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="houses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->house_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->house_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'house_id',
            'streetname',
            'housenumber',
            'addition',
            'city',
            'postalcode',
            'price',
            'floor',
            'parcel',
            'nbr_rooms',
            'broker_name',
            'broker_id',
            'hose_img',
            'hose_url:url',
            'date_ins',
            'date_upd',
            'object_id',
            'status',
        ],
    ]) ?>

</div>
