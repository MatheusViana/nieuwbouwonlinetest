<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Houses;

/**
 * HousesSearch represents the model behind the search form of `app\models\Houses`.
 */
class HousesSearch extends Houses
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['house_id', 'floor', 'parcel', 'nbr_rooms', 'object_id'], 'integer'],
            [['streetname', 'housenumber', 'addition', 'city', 'postalcode', 'price', 'broker_name', 'broker_id', 'hose_img', 'hose_url', 'date_ins', 'date_upd', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Houses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'house_id' => $this->house_id,
            'floor' => $this->floor,
            'parcel' => $this->parcel,
            'nbr_rooms' => $this->nbr_rooms,
            'date_ins' => $this->date_ins,
            'date_upd' => $this->date_upd,
            'object_id' => $this->object_id,
        ]);

        $query->andFilterWhere(['like', 'streetname', $this->streetname])
            ->andFilterWhere(['like', 'housenumber', $this->housenumber])
            ->andFilterWhere(['like', 'addition', $this->addition])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postalcode', $this->postalcode])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'broker_name', $this->broker_name])
            ->andFilterWhere(['like', 'broker_id', $this->broker_id])
            ->andFilterWhere(['like', 'hose_img', $this->hose_img])
            ->andFilterWhere(['like', 'hose_url', $this->hose_url])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
