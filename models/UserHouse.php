<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * CadastroModel is the model behind the contact form.
 */
class UserHouse extends Model
{
    public $postalcode;
    public $housenumber;
    public $housenumberaddition;
    public $squaremeter;

    public $reason;
    public $timetomove;
    public $typeofhouse;
    
    public $email;
    public $firstname;
    public $addition;
    public $lastname;
    public $phonenumber;
    
    public $presentedoption;

    public function rules()
    {
        return [
            [['postalcode', 'housenumber', 'housenumberaddition', 'squaremeter', 'reason', 'timetomove', 'typeofhouse', 
                'email', 'firstname', 'addition', 'lastname', 'phonenumber', 'presentedoption'], 'required'],
            ['email', 'email'],
            ['squaremeter', 'number', 'integerOnly' => true],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'postalcode' => 'Postal Code',
            'housenumber' => 'House Number',
            'housenumberaddition' => 'House Number Addition',
            'squaremeter' => 'Square Meters',
        
            'reason' => 'Reason',
            'timetomove' => 'When do you want to move',
            'typeofhouse' => 'What is you type of house',
            
            'email' => 'Email',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'addition' => 'Addition',            
            'phonenumber' => 'Phone Number',
            
            'presentedoption' => 'How do you want the report to be presented?',
        ];
    }

    public function sendEmail($emailContent){
        Yii::$app->mailer->compose('@app/mail/layouts/html', ['content' => $emailContent])
            ->setTo($model->email)
            ->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['sender']])
            ->setSubject('House price report')
            ->send();

        return true;
    }
}
