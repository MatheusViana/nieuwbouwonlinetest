<?php

namespace app\controllers;

use Yii;
use Mpdf\Mpdf;
use yii\web\Controller;
use yii\web\Response;
use app\models\Houses;
use app\models\UserHouse;

class UserhouseController extends Controller
{
    /**
     * Displays UserHouse form,
     * 
     * @return string
     */
    public function actionIndex(){
        $userHouse = new UserHouse();
        $post = Yii::$app->request->post();

        if($userHouse->load($post) && $userHouse->validate()){
            return Self::housesReport($userHouse);
        } else {
            return $this->render('index', [
                'model' => $userHouse
            ]);
        }
    }

    /**
     * Displays report houses searched,
     * 
     * @return string
     */
    public function housesReport($searchInfo){

        $postalnumber = $searchInfo->postalcode;
        $floor = $searchInfo->squaremeter;
        $houses = []; // stores an array of houses found
        $totalhouses = 10; // number of houses to be selected
        /*
            Is used when not enough houses with certain floor number is found, with the range
            houses with higher or lower range are added to the search, the lower the floorRange
            more accurate are the search
        */
        $floorRange = 10;    
        
        $houses = Self::searchHouses($postalnumber, $floor, $totalhouses, $floorRange);

        if(count($houses)>0){
            /*
                Creates an array with the price/m2 of each house found
            */
            $squarefloor = [];
            foreach($houses as $key => $house){
                array_push($squarefloor, ($house->price/$house->floor));
            }

            $pricePerFloor = round((array_sum($squarefloor)/count($squarefloor)), 2);
            $houseprice = round($pricePerFloor*$floor, 2);

            $viewData = [
                'houseprice' => $houseprice,
                'pricePerFloor' => $pricePerFloor,
                'houses' => $houses,
                'searchInfo' => $searchInfo
            ];

            if($searchInfo->presentedoption == "1"){ // user chosen pdf
                $mpdf = new Mpdf;
                $mpdf->WriteHTML($this->renderPartial('report', $viewData));
                $mpdf->Output();
            } else {
                if($searchInfo->sendEmail($this->renderPartial('report', $viewData))){
                    Yii::$app->session->setFlash('emailSubmitted');
                }        
        
                return $this->render('report', $viewData);
            }  
        } else {
            Yii::$app->session->setFlash('noHousesFound');
            return $this->render('report');
        }           
        
    }

    public function searchHouses($postalnumber, $floor, $totalhouses, $floorRange){
        $houses = [];
        for($i=4; $i>=2; $i--){            
            $housesFound = count($houses);
            if($housesFound<$totalhouses){
                $housesleft = $totalhouses-$housesFound;
    
                $postal = substr($postalnumber, 0, $i);
                $idHousesFound = array_column($houses, 'house_id');
                $housessearch = Self::searchHomesByFloor($postal, $floor, $housesleft, $idHousesFound);

                $houses = array_merge($houses, $housessearch);
            } else {
                break;
            }
        }

        if(count($houses)<$totalhouses){
            for($i=4; $i>=2; $i--){
                $housesFound = count($houses);
                if($housesFound<$totalhouses){                
                    $housesleft = $totalhouses-$housesFound;
        
                    $postal = substr($postalnumber, 0, $i);
                    $idHousesFound = array_column($houses, 'house_id');
                    $housessearch = Self::searchHomesByFloorRange($postal, $floor, $housesleft, $idHousesFound, $floorRange);
    
                    $houses = array_merge($houses, $housessearch);
                } else {
                    break;
                }
            }
        }
        return $houses;
    }

    public function actionGraphs(){
        return $this->render('graphs');
    }

    public function actionHousesfloor() {

        header('Content-Type: application/json');

        $list = Yii::$app->db->createCommand('select floor, count(house_id) as houses from houses group by floor order by floor asc')->queryAll();

        $labels = array_column($list, 'floor');
        $data = array_column($list, 'houses');
        
        $response['data'] = $data;
        $response['labels'] = $labels;

        return json_encode($response);
    }

    public function actionHouseprice() {

        header('Content-Type: application/json');

        $list = Yii::$app->db->createCommand('SELECT price, count(house_id) as houses FROM houses WHERE price REGEXP "^[0-9]+$" group by price ORDER BY ABS(price) ASC')->queryAll();

        $labels = array_column($list, 'price');
        $data = array_column($list, 'houses');
        
        $response['data'] = $data;
        $response['labels'] = $labels;

        return json_encode($response);
    }

    /**
     * Search houses using postal code and floor number,
     * 
     * @return object
     */
    public function searchHomesByFloor($postalcode, $floor, $limit, $arrayexcluded){
        return Houses::find()
            ->where('postalcode LIKE :substr', array(':substr' => $postalcode.'%'))
            ->andwhere(['floor' => $floor])
            ->andwhere(['not in','house_id',$arrayexcluded])            
            ->limit($limit)
            ->all();
    }

    /**
     * Search houses using postal code and floor number range,
     * sorting by closest floor number provided
     * @return object
     */
    public function searchHomesByFloorRange($postalcode, $floor, $limit, $arrayexcluded, $thereshold){
        return Houses::find()
            ->where('postalcode LIKE :substr', array(':substr' => $postalcode.'%'))            
            ->andwhere(['<=','floor',($floor+$thereshold)])
            ->andwhere(['>=','floor',($floor-$thereshold)])
            ->andwhere(['not in','house_id',$arrayexcluded])            
            ->limit($limit)
            ->orderBy('abs(floor - '.$floor.') ASC')
            ->all();
    }
}